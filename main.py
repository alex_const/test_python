import time


def sum(a, b):
    return a + b

def my_sleep(seconds):
    time.sleep(seconds)

if __name__ == '__main__':
    my_sleep(3)
    print("Just woke up")

