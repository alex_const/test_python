import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from main import sum

def test_sum():
    assert sum(1, 2) == 3
    assert sum(-1, -2) == -3
    assert sum(0, 0) == 0

